# Carlo Sanchez
**Date of birth:** 16/09/2002<br>
**Nationality:** Spanish<br>
**Address:** Sant Cugat del Vallès 08174, Catalunya, Spain<br>
**Phone number:** +34 645 623 157<br>
**Email address:** carlosanchezvilar2002@gmail.com<br>

---
### About me
```
I am a Cross-Platfrom Application Development: Video Games and Digital Entertainment student at the Institut
Tecnològic de Barcelona. I am someone who likes to work with computers and related things. I consider me
someone persistent, tidy and someone who likes to be good at what he does.

I am currently looking for internships.
```
---
## ```🎓 Education```
📅 2020 - PRESENT | 📍 Carrer d'Aiguablava 121, 08033 Barcelona (Spain)
*	### Cross-Platfrom Application Development
	 ### ITB (Institut Tecnològic de Barcelona)
	**Specialisation:** Video Games and Digital Entertainment<br>

📅 2018 - 2020 | 📍 Carrer de Granollers 43, 08173 Sant Cugat del Vallès (Spain)
*	### High School Degree
	### IES Angeleta Ferrer i Sensat
	**Specialisation:** Technological
---
## ```💻 Skills```
### ✔️ LANGUAGES
* Spanish - Native
* Catalan - Native
* Italian - Native
* English - B2
### ✔️ PROGRAMMING LANGUAGES
* Java
* C#
* Kotlin
* HTML/XML/CSS
* Python
* PL/pgSQL
### ✔️ TECHNOLOGIES, PROGRAMS
* IntelliJ IDEA
* Unity
* Visual Studio
* Visual Studio Code
* Android Studio
* Git
* Blender
* Krita
* Pycharm
---
## ```📊 Cognitive Traits```
* Learning and use of knowledge
* Self control
* Creativity
* Empathy
* Conceptual thinking
* Teamwork and cooperation
---
## ```🎮 Hobbies```
* **Video Games**
	* I spend most of my free time playing various video games, of any genre. They help me to distance myself from the stress that I may have due to a day-to-day situation. I really enjoy playing them.
* **Drawing**
	* I've liked to draw since I was a kid and I still love it. When inspiration comes to me, I always try to portray it if I have some free time.
* **Series**
	* It's not one of the hobbies I frequent the most but sometimes I start to watch new series and watch the new episodes when they come out, whether they are television series, Netflix or animation/anime.
